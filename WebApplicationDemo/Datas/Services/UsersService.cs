﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationDemo.Helper;
using WebApplicationDemo.Interface;
using WebApplicationDemo.Models.Config;
using WebApplicationDemo.Models.Entities;
using WebApplicationDemo.Models.Request;
using WebApplicationDemo.Models.Response;

namespace WebApplicationDemo.Datas.Services
{
    public class UsersService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private HelperClass _helperClass;
        private Appsetting _option;
        private readonly SessionDataModel _sessionData;

        public UsersService(ApplicationDbContext applicationDbContext,IOptions<Appsetting> options, ISessionData sessionData)
        {
            _applicationDbContext = applicationDbContext;
            _helperClass = new HelperClass();
            _option = options.Value;
            _sessionData = sessionData.sessionData;
        }

        public async Task<UserViewModel> GetUser(LoginRequest request) 
        {
            var users = await _applicationDbContext.Users.Where(x => x.Username == request.username && x.Password == request.password)
                .Join(
                    _applicationDbContext.Roles,
                    users => users.RoleId,
                    roles => roles.RoleId,
                    (user,role) => new UserViewModel
                    { 
                        UserId = user.UserId,
                        Firstname = user.Username,
                        Lastname = user.Lastname,
                        Rolename = role.RoleName,
                        Username = user.Username,
                    }
                )
                .FirstOrDefaultAsync();
            if (users == null) throw new Exception("ชื่อผู้ใช้งานไม่ถูกต้อง");
            string token = _helperClass.GenerateToken(users.UserId, users.Username, _option.EXPIRE_DATE, _option.JWT_SECRETKEY);
            users.Token = token;
            return users;
        }

        public async Task<UserViewModel> GetMe(int userId)
        {
            var users = await _applicationDbContext.Users.Where(x => x.UserId == _sessionData.userId)
                .Join(
                    _applicationDbContext.Roles,
                    users => users.RoleId,
                    roles => roles.RoleId,
                    (user, role) => new UserViewModel
                    {
                        UserId = user.UserId,
                        Firstname = user.Username,
                        Lastname = user.Lastname,
                        Rolename = role.RoleName,
                        Username = user.Username,
                    }
                )
                .FirstOrDefaultAsync();
            if (users == null) throw new Exception("ชื่อผู้ใช้งานไม่ถูกต้อง");
            string token = _helperClass.GenerateToken(users.UserId, users.Username, _option.EXPIRE_DATE, _option.JWT_SECRETKEY);
            users.Token = token;
            return users;
        }

        public async Task<List<UserViewModel>> GetUserList()
        {
            var users = await _applicationDbContext.Users
                .Join(
                    _applicationDbContext.Roles,
                    users => users.RoleId,
                    roles => roles.RoleId,
                    (user, role) => new UserViewModel
                    {
                        Firstname = user.Username,
                        Lastname = user.Lastname,
                        Rolename = role.RoleName,
                        Username = user.Username
                    }
                )
                .ToListAsync();
            if (users == null) throw new Exception("ชื่อผู้ใช้งานไม่ถูกต้อง");
            return users;
        }

        public async Task<User> AddUser(UserRequest request)
        {
            var user = new User
            {
                Firstname = request.Firstname,
                Lastname = request.Lastname,
                Password = request.Password,
                RoleId = request.RoleId,
                Username = request.Username
            };
            _applicationDbContext.Users.Add(user);
            await _applicationDbContext.SaveChangesAsync();
            return user;
        }

        public async Task<User> EditUser(int userId, UserRequest request)
        {
            var user = _applicationDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
            user.Firstname = request.Firstname;
            user.Lastname = request.Lastname;
            user.Password = request.Password;
            user.RoleId = request.RoleId;
            _applicationDbContext.Users.Update(user);
            await _applicationDbContext.SaveChangesAsync();
            return user;
        }

        public async Task DeleteUser(int userId)
        {
            var user = _applicationDbContext.Users.Where(x => x.UserId == userId).FirstOrDefault();
            if (user == null) throw new Exception("ไม่มีข้อมูลผู้ใช้งาน");
            _applicationDbContext.Users.Remove(user);
            await _applicationDbContext.SaveChangesAsync();
        }
    }

}

