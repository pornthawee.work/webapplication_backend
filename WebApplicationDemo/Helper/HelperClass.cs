﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationDemo.Helper
{
    public class HelperClass
    {
        public string GenerateToken(long userId, string username, int ExpiresDate, string Secret)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));
            var tokenHandler = new JwtSecurityTokenHandler();

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]{
                    new Claim("userID", userId.ToString()),
                    new Claim("username", username),
                     }),
                Expires = DateTime.Now.AddDays(ExpiresDate),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        public JwtSecurityToken ReadJwtToken(string token)
        {
            JwtSecurityToken data;
            if (!string.IsNullOrEmpty(token))
            {
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                data = jsonToken as JwtSecurityToken;
            }
            else
            {
                data = null;
            }
            return data;
        }
    }
}
