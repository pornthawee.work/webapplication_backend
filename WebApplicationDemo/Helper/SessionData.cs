﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationDemo.Interface;
using WebApplicationDemo.Models.Config;

namespace WebApplicationDemo.Helper
{
    public class SessionData : ISessionData
    {
        public SessionDataModel sessionData { get; private set; }
        private readonly HelperClass _helper = new HelperClass();
        public SessionData(IActionContextAccessor accessor)
        {
            var httpContext = accessor.ActionContext.HttpContext;
            var token = httpContext.GetTokenAsync("access_token").Result;
            var dataToken = _helper.ReadJwtToken(token);
            sessionData = new SessionDataModel();
            if (dataToken != null)
            {
                var userID = dataToken.Payload["userID"];
                var username = dataToken.Payload["username"];
                sessionData.userId = Convert.ToInt16(userID);
                sessionData.username = username.ToString();
            }
        }
    }
}
