﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationDemo.Models.Config;

namespace WebApplicationDemo.Interface
{
    public interface ISessionData
    {
        SessionDataModel sessionData { get; }

    }
}
