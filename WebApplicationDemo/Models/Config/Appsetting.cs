﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationDemo.Models.Config
{
    public class Appsetting
    {
        public string JWT_SECRETKEY { set; get; }
        public int EXPIRE_DATE { set; get; }
    }
}
