﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationDemo.Models.Config
{
    public class SessionDataModel
    {
        public int userId { set; get; }
        public string username { set; get; }
    }
}
