﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplicationDemo.Models.Entities
{
    public partial class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public int? RoleId { get; set; }
    }
}
